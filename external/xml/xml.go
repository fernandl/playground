package xml

import (
	"bytes"
	"encoding/xml"

	"golang.org/x/net/html/charset"
)

type XMLParser struct{}

func NewXMLParser() XMLParser {

	return XMLParser{}
}

func (xp XMLParser) Parse(data []byte, v interface{}) (interface{}, error) {

	reader := bytes.NewReader(data)
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(v)

	if err != nil {
		return nil, err
	}

	return v, nil
}
