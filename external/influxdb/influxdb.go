package influxdb

import (
	"log"
	"time"

	"github.com/influxdata/influxdb/client/v2"
)

type Influx struct {
	Client      client.Client
	BatchPoints client.BatchPoints
	Tags        map[string]string
}

func NewInflux(cl client.Client, db string, tags map[string]string) Influx {

	bp := newBatchPoints(db)

	influx := Influx{
		Client:      cl,
		BatchPoints: bp,
		Tags:        tags,
	}

	return influx
}

func newBatchPoints(database string) client.BatchPoints {

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  database,
		Precision: "s",
	})

	if err != nil {
		log.Println(err)
	}

	return bp
}

func (in Influx) Store(f float32) error {

	pt, err := in.newPoint(f)

	if err != nil {
		return err
	}

	in.BatchPoints.AddPoint(pt)

	err = in.Client.Write(in.BatchPoints)

	if err != nil {
		return err
	}

	return nil
}

func (in Influx) newPoint(f float32) (*client.Point, error) {

	tags := in.Tags

	fields := map[string]interface{}{
		"temperature": f,
	}

	pt, err := client.NewPoint("temperature", tags, fields, time.Now())

	if err != nil {
		return nil, err
	}

	return pt, nil
}

// type In
