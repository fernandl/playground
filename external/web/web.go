package web

import (
	"io/ioutil"
	"net/http"
)

type Caller struct {
	URL string
}

func NewCaller(url string) Caller {

	caller := Caller{
		URL: url,
	}

	return caller
}

func (c Caller) Get() ([]byte, error) {
	resp, err := http.Get(c.URL)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	return body, nil
}
