package domain

type Temperature struct {
	Temperature float32 `xml:"temperature"`
}

type TemperatureSource interface {
	Get() ([]byte, error)
}

type TemperatureParser interface {
	Parse([]byte, interface{}) (interface{}, error)
}

type TemperatureRepository interface {
	Store(float32) error
}

type TemperatureInteractor struct {
	TemperatureSource
	TemperatureParser
	TemperatureRepository
}

func NewTemperatureInteractor() *TemperatureInteractor {
	return &TemperatureInteractor{}
}

func (ti TemperatureInteractor) GetParseStore() {

	t := Temperature{}

	data, _ := ti.Get()

	// if err != nil {
	// 	return err
	// }

	temp, _ := ti.Parse(data, &t)

	// if err != nil {
	// 	return err
	// }

	ti.Store(temp.(*Temperature).Temperature)

	// if err != nil {
	// 	return err
	// }

}
