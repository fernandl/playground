package main

import (
	"log"

	"github.com/influxdata/influxdb/client/v2"
	"github.com/robfig/cron"

	"gitlab.cern.ch/fernandl/playground/domain"
	"gitlab.cern.ch/fernandl/playground/external/influxdb"
	"gitlab.cern.ch/fernandl/playground/external/web"
	"gitlab.cern.ch/fernandl/playground/external/xml"
)

const thermoURL string = "http://thermometer03.cern.ch/tme.xml"
const logFile string = "thermo.log"

const (
	addr     = "http://test-room-temp.web.cern.ch:80"
	database = "itshot"
	username = "thermo"
	password = "papouch"
)

func main() {

	temperatureInteractor := domain.NewTemperatureInteractor()

	client := newinfluxClient()

	influx := influxdb.NewInflux(client, database, map[string]string{"temperature": "31/1-18-temperature"})
	xmlParser := xml.NewXMLParser()
	caller := web.NewCaller(thermoURL)

	temperatureInteractor.TemperatureSource = caller
	temperatureInteractor.TemperatureParser = xmlParser
	temperatureInteractor.TemperatureRepository = influx

	scheduler := cron.New()

	scheduler.AddFunc("@every 10s", temperatureInteractor.GetParseStore)
	scheduler.Start()

	block := make(chan struct{})
	<-block

}

func newinfluxClient() client.Client {
	cl, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     addr,
		Username: username,
		Password: password,
	})

	if err != nil {
		log.Fatalln("Error: ", err)
	}

	return cl

}
